import {DATA_PATH} from 'config';
import fs from 'mz/fs';
import path from 'path';

const getDataPath = (id) => path.join(DATA_PATH, id);
const getPhotoPath = (id, type) => {
  if (type === 'source') {
    return path.join(DATA_PATH, id, type);
  }
  return path.join(DATA_PATH, id, `${type}.jpg`);
};

const ensurePath = async (id) => {
  const dataPath = getDataPath(id);
  const dataPathStat = await fs.stat(dataPath).catch(() => null);

  if (!(dataPathStat && dataPathStat.isDirectory())) {
    await fs.mkdir(dataPath);
  }
};

const move = async (id, type, filePath) => {
  const photoPath = getPhotoPath(id, type);

  await ensurePath(id);
  await fs.copyFile(filePath, photoPath);
  await fs.unlink(filePath);
};

const get = (id, type) => {
  const photoPath = getPhotoPath(id, type);

  return fs.readFile(photoPath);
};

const getPath = (id, type) => getPhotoPath(id, type);

const remove = (id, type) => {
  const photoPath = getPhotoPath(id, type);

  return fs.unlink(photoPath);
};

export default {
  get,
  getPath,
  move,
  remove,
};
