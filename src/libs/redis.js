import redis from 'bootstrap/redis';

const hget = async (key, id) => {
  const data = await redis.hgetAsync(key, id);

  return JSON.parse(data || 'null');
};

const hset = (key, id, data) =>
  redis.hsetAsync(key, id, JSON.stringify(data));

export default {hget, hset};
