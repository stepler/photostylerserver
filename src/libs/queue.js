import queue from 'bootstrap/queue';

const createTask = (id) => {
  queue
    .createJob()
    .setId(id)
    .save();
};

const processTasks = (processFunc) => {
  queue.process((job, done) => {
    const progressCb = (percents) => {
      job.reportProgress(percents);
    };

    processFunc(job.id, progressCb)
      .then((data) => {
        done(null, data);
      })
      .catch((err) => {
        done(err);
      });
  });
};

const subscribeProgressTasks = (callback) => {
  queue.on('job progress', (jobId, result) => {
    callback(jobId, result);
  });
};

const subscribeSucceededTasks = (callback) => {
  queue.on('job succeeded', (jobId, result) => {
    callback(jobId, result);
  });
};

const subscribeFailedTasks = (callback) => {
  queue.on('job failed', (jobId, result) => {
    callback(jobId, result);
  });
};

export default {
  createTask,
  processTasks,
  subscribeProgressTasks,
  subscribeSucceededTasks,
  subscribeFailedTasks,
};
