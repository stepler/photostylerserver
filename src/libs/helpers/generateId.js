export const generateId = () => {
  const prefix = Date.now().toString().slice(-4);
  const suffix = parseInt(Math.random() * 1000).toString();

  return Buffer
    .from(`${prefix}${suffix}`)
    .toString('base64')
    .replace(/[^\w]/g, '');
};

