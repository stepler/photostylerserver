import {
  PHOTO_STATUS_FAILED,
  PHOTO_STATUS_SUCCESS,
  REDIS_PHOTOS_KEY,
} from 'config';
import {getPhotoRecord} from 'libs/helpers';
import queue from 'libs/queue';
import redis from 'libs/redis';

export const savePhotos = () => {
  queue.subscribeSucceededTasks((photoId, result) => {
    const data = getPhotoRecord(
      PHOTO_STATUS_SUCCESS,
      result
    );

    redis.hset(REDIS_PHOTOS_KEY, photoId, data);
  });


  queue.subscribeFailedTasks(async (photoId) => {
    const data = getPhotoRecord(PHOTO_STATUS_FAILED);

    redis.hset(REDIS_PHOTOS_KEY, photoId, data);
  });
};
