import {
  PHOTO_FILTERS,
  PHOTO_MAX_HEIGHT,
  PHOTO_MAX_WIDHT,
} from 'config';
import logger from 'libs/logger';
import queue from 'libs/queue';
import sharp from 'sharp';
import storage from 'libs/storage';
import styler from 'libs/styler';

const PROGRESS_STEP = 100 / (PHOTO_FILTERS.length + 1);

const processPhotoCallback = async (photoId, progressCb) => {
  try {
    logger.info(`Process: Start ${photoId}`);

    let progress = 0;
    const sourcePath = storage.getPath(photoId, 'source');
    const originalPath = storage.getPath(photoId, 'original');

    await sharp(sourcePath)
      .resize(PHOTO_MAX_WIDHT, PHOTO_MAX_HEIGHT)
      .min()
      .jpeg()
      .toFile(originalPath);

    progressCb(progress += PROGRESS_STEP);
    logger.debug(`Process: Save original ${photoId}`);

    for (const filter of PHOTO_FILTERS) {
      const styledPath = await styler(originalPath, filter);

      await storage.move(photoId, filter, styledPath);
      progressCb(progress += PROGRESS_STEP);
      logger.debug(`Process: Save ${filter} ${photoId}`);
    }

    logger.info(`Process: Done ${photoId}`);

    return {
      original: storage.getPath(photoId, 'original'),
      filtered: PHOTO_FILTERS.map((type) => ({
        type,
        src: storage.getPath(photoId, type),
      })),
    };
  } catch (e) {
    logger.error(`Process: Unable to process ${photoId} '${e.message}'`);
  }
};

export const processPhotos = () => {
  queue.processTasks(processPhotoCallback);
};
