import {
  PHOTO_STATUS_FAILED,
  REDIS_PHOTOS_KEY,
} from 'config';
import {getPhotoRecord} from 'libs/helpers';
import redis from 'libs/redis';

export const getPhoto = async (photoId) => {
  const data = await redis.hget(REDIS_PHOTOS_KEY, photoId);

  if (!data) {
    return getPhotoRecord(PHOTO_STATUS_FAILED);
  }

  return data;
};
