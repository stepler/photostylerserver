import {
  PHOTO_STATUS_PROCESS,
  REDIS_PHOTOS_KEY,
} from 'config';
import {
  generateId,
  getPhotoRecord,
} from 'libs/helpers';
import queue from 'libs/queue';
import redis from 'libs/redis';
import storage from 'libs/storage';

export const addPhoto = async (filePath) => {
  const photoId = generateId();
  const data = getPhotoRecord(PHOTO_STATUS_PROCESS);

  await redis.hset(REDIS_PHOTOS_KEY, photoId, data);
  await storage.move(photoId, 'source', filePath);

  queue.createTask(photoId);

  return photoId;
};
