import {STYLE_TRANSFER_SRC} from 'config';
import fs from 'mz/fs';
import os from 'os';
import path from 'path';
import shell from 'shelljs';

const getTmpFile = (sourceFile, model) => {
  const tmpdir = os.tmpdir();
  const base = path.basename(sourceFile);

  return `${tmpdir}/${model}.${base}`;
};

const getExecCmd = (sourceFile, destFile, model) => {
  const src = STYLE_TRANSFER_SRC;

  return `cd ${src} && \
    python evaluate.py \
      --checkpoint models/${model}.ckpt \
      --in-path ${sourceFile} \
      --out-path ${destFile}`;
};

const transferStyle = (sourceFile, destFile, model) => {
  const cmd = getExecCmd(sourceFile, destFile, model);

  return new Promise((resolve, reject) => {
    shell.exec(cmd, (code, stdout, stderr) => {
      const osx = require('os');

      if (code === 0) {
        resolve(stdout);
      } else {
        reject(stderr);
      }
    });
  });
};

export default async (sourceFile, model) => {
  const destFile = getTmpFile(sourceFile, model);

  await transferStyle(sourceFile, destFile, model);

  return destFile;
};
