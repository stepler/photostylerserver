import {generateId} from 'libs/helpers';
import multer from 'multer';
import os from 'os';

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, os.tmpdir()),
  filename: (req, file, cb) => cb(null, generateId()),
});

export default multer({storage});
