import express from 'express';

const ARGS_NEXT = 2;

const catchError = (fn) =>
  (...args) => fn(...args).catch(args[ARGS_NEXT]);

const wrapHandlers = (args) =>
  args.slice(0, -1).concat(
    args.slice(-1).map(catchError)
  );

export default () => {
  const app = express();

  ['all', 'get', 'post', 'put', 'delete'].forEach((method) => {
    const originMethod = app[method];

    app[method] = (...args) => originMethod.apply(app, wrapHandlers(args));
  });

  return app;
};
