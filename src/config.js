/* eslint-disable no-process-env */

export const {NODE_ENV = 'production'} = process.env;
export const IS_PRODUCTION = NODE_ENV === 'production';

export const {DATA_PATH} = process.env;
export const {WEB_DATA_PATH} = process.env;
export const {REDIS_URL} = process.env;
export const {LOGS_PATH} = process.env;
export const {STYLE_TRANSFER_SRC} = process.env;

export const PHOTO_MAX_WIDHT = 200;
export const PHOTO_MAX_HEIGHT = 200;
export const PHOTO_FILTERS = [
  'wreck',
  'wave',
  'udnie',
  'scream',
  'rain_princess',
  'la_muse',
];

export const HTTP_OK = 200;
export const HTTP_BAD_REQUEST = 400;
export const HTTP_NOT_FOUND = 404;
export const HTTP_ERROR = 500;

export const REDIS_PHOTOS_KEY = 'photos';
export const REDIS_PROCESS_KEY = 'process';

export const PHOTO_STATUS_PROCESS = 'process';
export const PHOTO_STATUS_SUCCESS = 'success';
export const PHOTO_STATUS_FAILED = 'failed';
