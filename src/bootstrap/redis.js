import {REDIS_URL} from 'config';
import bluebird from 'bluebird';
import redis from 'redis';

bluebird.promisifyAll(redis.RedisClient.prototype);

const client = redis.createClient({url: REDIS_URL});

export default client;
