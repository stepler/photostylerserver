import {
  IS_PRODUCTION,
  LOGS_PATH,
} from 'config';
import log4js from 'log4js';

const layout = {
  type: 'pattern',
  pattern: '%d{yyyy-MM-dd hh:mm:ss} [%p] %m',
};

log4js.configure({
  appenders: {
    file: {
      type: 'dateFile',
      filename: `${LOGS_PATH}/app.log`,
      pattern: '.yyyy-MM',
      alwaysIncludePattern: true,
      layout,
    },
    console: {
      type: 'console',
      layout,
    },
  },
  categories: {
    default: {
      appenders: [
        IS_PRODUCTION
          ? 'file'
          : 'console',
      ],
      level: IS_PRODUCTION
        ? 'info'
        : 'debug',
    },
  },
});

export default log4js.getLogger();
