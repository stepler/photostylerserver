import {REDIS_PROCESS_KEY, REDIS_URL} from 'config';
import Queue from 'bee-queue';
import url from 'url';

const {hostname, port} = url.parse(REDIS_URL);

const queue = new Queue('photos', {
  prefix: REDIS_PROCESS_KEY,
  delayedDebounce: 120 * 1000,
  redis: {
    host: hostname,
    port,
  },
  isWorker: true,
});

export default queue;
