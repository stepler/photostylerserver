import {HTTP_NOT_FOUND} from 'config';

export const handler404 = (req, res) =>
  res.sendStatus(HTTP_NOT_FOUND);
