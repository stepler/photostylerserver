import {HTTP_ERROR} from 'config';
import logger from 'libs/logger';

export const handler500 = (err, req, res, next) => {
  logger.error(err.stack);
  res.sendStatus(HTTP_ERROR);
};
