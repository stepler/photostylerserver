import logger from 'libs/logger';

export const handlerError = (err) =>
  logger.error(err.stack);
