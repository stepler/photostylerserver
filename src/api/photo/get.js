import {
  HTTP_NOT_FOUND,
  PHOTO_STATUS_FAILED,
} from 'config';
import {getPhoto} from 'libs/services';

export const get = async (req, res) => {
  const {id} = req.params;

  const photo = await getPhoto(id);

  if (photo.status === PHOTO_STATUS_FAILED) {
    res.status(HTTP_NOT_FOUND);
  }

  res.json(photo);
};
