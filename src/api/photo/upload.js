import {addPhoto} from 'libs/services';
import {HTTP_BAD_REQUEST} from 'config';

export const upload = async (req, res) => {
  const {file} = req;

  if (!(file && file.path)) {
    res.sendStatus(HTTP_NOT_FOUND);
    return;
  }

  const id = await addPhoto(file.path);

  res.json({id});
};
