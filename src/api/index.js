import * as photo from './photo/index';
import * as system from './system/index';

export {
  photo,
  system,
};
