import 'babel-polyfill';
import 'bootstrap/index';
import {
  photo,
  system,
} from 'api/index';
import bodyParser from 'body-parser';
import express from 'libs/express';
import logger from 'libs/logger';
import {savePhotos} from 'libs/process/index';
import uploadStorage from 'libs/uploadStorage';

const app = express();

// app.use(bodyParser.raw());
app.use(bodyParser.json());

app.get('/api/v1/photo/:id', photo.get);
app.post('/api/v1/photo', uploadStorage.single('file'), photo.upload);

app.use(system.handler404);
app.use(system.handler500);

process.on('uncaughtException', system.handlerError);
process.on('unhandledRejection', system.handlerError);

app.listen('8000', '0.0.0.0');

logger.debug('Running on http://0.0.0.0:8000');
savePhotos();
