FROM tensorflow/tensorflow

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
  apt-get update -qq && \
  apt-get install --no-install-recommends -y nodejs build-essential && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /app

COPY ./style_transfer /style_transfer

COPY ./package.json /app
COPY ./.babelrc /app
COPY ./src /app/src

RUN npm install

EXPOSE 80
